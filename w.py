from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from flask import Flask
from flask import jsonify
from flask import url_for
import  threading
from flask import abort
from flask import make_response
from flask import request
from datetime import date, datetime
import tensorflow as tf
import argparse
import sys
import est_data
import mysql.connector
import json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


app = Flask(__name__)
parser = argparse.ArgumentParser()
parser.add_argument('--batch_size', default=100, type=int, help='batch size')
parser.add_argument('--train_steps', default=1000, type=int,
                    help='number of training steps')

db=mysql.connector.connect(host='127.0.0.1',   
                     user='root',         
                     password='tano',  
                     database='predisoft')

fromaddr = 'predisoftsoftware@gmail.com'
username = 'predisoftsoftware@gmail.com'
password = 'PredisoftSSE'
@app.route("/api/prediccion", methods=["GET"])
def listarAdultos():
    cur = db.cursor()
    cur.execute("select p.nombre, p.cedula from persona p join adultomayor a on a.persona_cedula=p.cedula where a.persona_cedula not in (select p.adultomayor_persona_cedula from prueba p where p.fecha=%s)",(datetime.now().date(),))   
    adultos=[]   
   
    for row in cur.fetchall():
        adultos.append({"nombre":row[0],"cedula":row[1]}) 
    cur.close()
    return json.dumps(adultos)
   
   
@app.route("/api/prediccion", methods=["POST"])
def cargarDatos():
       if request.is_json:      
         colorUno=float(request.json["colorUno"])
         colorDos=float(request.json["colorDos"])
         colorTres=float(request.json["colorTres"])
         adultoMayor=request.json["adultoMayor"]
         hiloPrediccion=threading.Thread(name="hilo_Prediccion",target=realizarPrediccion,args=(colorUno,colorDos,colorTres,adultoMayor))
		 hiloPrediccion.start()
         hiloPrediccion.join()
         return jsonify({"resultado": "200"
                            })
       else:
          return jsonify({"Bad Request": "400"
                            })
def realizarPrediccion(colorUno,colorDos,colorTres,adultoMayor): 
          
     (train_x, train_y) = est_data.load_data()
     my_feature_columns = []
     for key in train_x.keys():
         my_feature_columns.append(tf.feature_column.numeric_column(key=key))
     classifier = tf.estimator.DNNClassifier(
            feature_columns=my_feature_columns,
             hidden_units=[10, 10],
             n_classes=2)
     classifier.train(
             input_fn=lambda:est_data.train_input_fn(train_x, train_y, 100),
             steps=1000)
     predict_x = {
            'colorUno': [colorUno],
            'colorDos': [colorDos],
            'colorTres': [colorTres],
           }

     predictions = classifier.predict(
             input_fn=lambda:est_data.eval_input_fn(predict_x,
                                            labels=None,
                                                batch_size=100))
     for pred_dict in predictions:
         class_id = pred_dict['class_ids'][0]
         probability = pred_dict['probabilities'][class_id]
         hiloInsercion=threading.Thread(name="hilo_Insercion",target=insercionPruebaBD,args=(float(probability),int(class_id),adultoMayor))
         hiloInsercion.start()
         hiloCorreo=threading.Thread(name="hilo_enviarCorreo",target=enviarCorreo,args=(adultoMayor,int(class_id),float(probability)))
         hiloCorreo.start()   
         hiloInsercion.join()
def insercionPruebaBD(resultado, estadoanimo_id, adultomayor_persona_cedula):
     cur=db.cursor()
     agregarRegistro=("insert into prueba "
                     "(fecha,resultado,estadoanimo_id,adultomayor_persona_cedula) "
                      "values(%s,%s,%s,%s)")
     datosPrueba=(datetime.now().date(),resultado,estadoanimo_id, adultomayor_persona_cedula)
     cur.execute(agregarRegistro, datosPrueba )
     cur.close()
     db.commit()

def enviarCorreo(adultoMayor,class_id,probability):

     if class_id==1 and probability>0.90:
         cur=db.cursor()
         curs=db.cursor()
         curs.execute("select p.nombre from persona p where p.cedula=%s",(adultoMayor,))
         nombre=str(curs.fetchone()[0])      
         curs.close()  
         server = smtplib.SMTP('smtp.gmail.com:587')
         server.ehlo()
         server.starttls()
         server.login(username,password)
         msg = MIMEMultipart()
         msg['From'] = fromaddr
         msg['Subject'] ="PrediSoft"
         body = "El adulto mayor "+nombre+" identificado con CC."+adultoMayor+" esta presentando un bajo estado de animo."    
         cur.execute("select f.correo from familiar f where f.adultomayor_persona_cedula=%s Union select p.correo from psicologo p",(adultoMayor,)) 
         for row in cur.fetchall():
             msg['To'] = row[0]
             msg.attach(MIMEText(body, 'plain'))
             text = msg.as_string()
             server.sendmail(fromaddr, row[0], text)
         server.quit()
         cur.close()
     
if __name__ == '__main__':
    app.debug=True
    app.run(host="0.0.0.0",port=9080)	 
			
 
